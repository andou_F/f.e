import java.io.*;
import java.util.*;

public class Main {
   public static void main(String[] args) throws Exception {
        int hp;     //  Hit Point
        int at;     // Attack
        int de;     // Defense
        int ag;     // Agility

        // ゲームの説明を表示
        System.out.println("【ジャバ闘技場へようこそ】");
        System.out.println("ここはいかに低いステータスで３人の剣闘士と死闘を繰り広げ");
        System.out.println("勝ち抜くことができるかというゲームです。");
        System.out.println("HPが0、または7ターン以内に倒せなかった場合、敗北となります。");
        System.out.println();
        System.out.println("-- Enterを押すとステータス入力に進みます --");
          Scanner scan = new Scanner(System.in);
          scan.nextLine();

        // 主人公の名前を入力
        System.out.println("名前を入力してください");
        String name = new java.util.Scanner(System.in).nextLine();
        System.out.println("あなたの名前は" + name + "で登録されました！");
        Music.sound("magic-cure1.wav");
        System.out.println();

        // ステータスの入力
        System.out.println("-- 自分のステータスを10〜99の半角数字で入力してください --");
        try {
          // HP
          System.out.println("１．HPを入力してください");
          System.out.println("あなたの体力です。HPが「0」になるとゲームオーバーです。");

          //ここから繰り返し開始
          while(true) {
              BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
              String str = br.readLine();
              if (str.matches("[0-9]{1,}")) {  // 入力文字が数字か数字以外かのチェック
                  hp = Integer.parseInt(str); //入力された値をint型に変換
                  if((hp > 9 && hp < 100)) {  //10以上99以下であれば、繰り返しを終了
                       break;
                  } else {
                      System.out.println("!! 10〜99の間で入力してください !!");//それ以外の場合は、処理を繰り返し
                  }
              } else {
                  System.out.println("!! 半角数字以外が入力されました。10〜99の半角数字で入力してください !!");
              }
          }
          System.out.println("-- あなたが入力したHPは " + hp + " です --");
          Music.sound("magic-cure1.wav");
          System.out.println();

          // 攻撃力
          System.out.println("２．攻撃力を入力してください");
          System.out.println("高ければ高いほど与えるダメージが増えます。");

          //ここから繰り返し開始
          while(true) {
              BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
              String str = br.readLine();
              if (str.matches("[0-9]{1,}")) {  // 入力文字が数字か数字以外かのチェック
                  at = Integer.parseInt(str); //入力された値をint型に変換
                  if((at > 9 && at < 100)) {  //10以上99以下であれば、繰り返しを終了
                       break;
                  } else {
                      System.out.println("!! 10〜99の間で入力してください !!");//それ以外の場合は、処理を繰り返し
                  }
              } else {
                  System.out.println("!! 半角数字以外が入力されました。10〜99の半角数字で入力してください !!");
              }
          }
          System.out.println("-- あなたが入力した攻撃力は " + at + " です --");
          Music.sound("magic-cure1.wav");
          System.out.println();

          // 守備力
          System.out.println("３．守備力を入力してください");
          System.out.println("相手の攻撃を軽減し、相手の攻撃力を自分の守備力が上回ればダメージは「0」にできます。");

          //ここから繰り返し開始
          while(true) {
              BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
              String str = br.readLine();
              if (str.matches("[0-9]{1,}")) {  // 入力文字が数字か数字以外かのチェック
                  de = Integer.parseInt(str); //入力された値をint型に変換
                  if((de > 9 && de < 100)) {  //10以上99以下であれば、繰り返しを終了
                       break;
                  } else {
                      System.out.println("!! 10〜99の間で入力してください !!");//それ以外の場合は、処理を繰り返し
                  }
              } else {
                  System.out.println("!! 半角数字以外が入力されました。10〜99の半角数字で入力してください !!");
              }
          }
          System.out.println("-- あなたが入力した守備力は " + de + " です --");
          Music.sound("magic-cure1.wav");
          System.out.println();

          // すばやさ
          System.out.println("４．すばやさを入力してください");
          System.out.println("相手より高ければ先に攻撃ができ、相手より5以上高かった場合「追撃」が発生します。");

          //ここから繰り返し開始
          while(true) {
              BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
              String str = br.readLine();
              if (str.matches("[0-9]{1,}")) {  // 入力文字が数字か数字以外かのチェック
                  ag = Integer.parseInt(str); //入力された値をint型に変換
                  if((ag > 9 && ag < 100)) {  //10以上99以下であれば、繰り返しを終了
                       break;
                  } else {
                      System.out.println("!! 10〜99の間で入力してください !!");//それ以外の場合は、処理を繰り返し
                  }
              } else {
                  System.out.println("!! 半角数字以外が入力されました。10〜99の半角数字で入力してください !!");
              }
          }
          System.out.println("-- あなたが入力したすばやさは " + ag + " です --");
          Music.sound("magic-cure1.wav");
          System.out.println();
          System.out.println("| HP：" + hp + " | " + "攻撃力：" + at + " | "
                              + "守備力：" + de + " | " + "すばやさ：" + ag + " |");
          System.out.println();

          int status = hp+at+de+ag;  // ステータスの合計

          for(int n = 0; n < 3; n++) {
             hp = Battle.Battle(name, hp, at, de, ag, n);  // hp上書き
             if(hp == 0){
             System.out.println("GAME OVER...");
             break;
          } else if(n == 2 && hp > 0) {
             System.out.println("すべての敵を倒しました！GAME CLEAR！");
             System.out.println("-- " + name + "のステータスの合計値は " + status +" です --");  // ステータスの合計表示
          }
          }

      } catch (Exception e) {
          System.out.println(e);
      }
   }
}