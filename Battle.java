public class Battle{
   
	public static int Battle(String name,int hp,int a,int b,int s,int n){
    	
		System.out.println((n+1)+"戦目");
		tm();
		Character t;
		String aru = name;
		String aru2 = "アルフォンス";
		if(aru.equals(aru2)){
			switch(n){
		    	case 0:
		        t=new Teki3(1);
		    	break;
				case 1:
		        t=new Teki4(1);
			    break;
				case 2:
		        t=new Teki5(1);
			    break;
				default:
					t=new Teki1(1);
			}
		}else{
			switch(n){
		    	case 0:
		        t=new Teki0(1);
		    	break;
				case 1:
		        t=new Teki1(1);
			    break;
				case 2:
		        t=new Teki2(1);
			    break;
				default:
					t=new Teki1(1);
			}
		}
		
		System.out.println("相手："+t.e_name+"\n");
		tm();
		String at="の攻撃：";
		String sp="の追撃：";
		
		
		for(int i=0;i<7;i++){
			System.out.println((i+1)+"ターン目\n");
			tm();
			if(s>=t.e_s){//自分の先制
				t.e_hp=atc(a,t.e_b,t.e_hp,name,at);  //自分の攻撃
				Music.sound("my.wav");
				if(t.e_hp==0){                      //HPの判定
					break;
				}
				tm();
				hp=atc(t.e_a,b,hp,t.e_name,at);    //相手の攻撃
				t.e_Sound();
				if(hp==0){                    //HPの判定
					break;
				}
				tm();
				if(s>(t.e_s+5)){//自分の追撃
					t.e_hp=atc(a,t.e_b,t.e_hp,name,sp);//自分の攻撃
					Music.sound("my.wav");
					if(t.e_hp==0){                      //HPの判定
						break;
					}
					tm();
				}
				
			}else{//相手の先制
				hp=atc(t.e_a,b,hp,t.e_name,at);    //相手の攻撃
				t.e_Sound();
				if(hp==0){                      //HPの判定
					break;
				}
				tm();
				t.e_hp=atc(a,t.e_b,t.e_hp,name,at);  //自分の攻撃
				Music.sound("my.wav");
				if(t.e_hp==0){                    //HPの判定
					break;
				}
				tm();
				if(t.e_s>(s+5)){//相手の追撃
			    	hp=atc(t.e_a,b,hp,t.e_name,sp);//相手の攻撃
					t.e_Sound();
					if(hp==0){                  //HP判定
					break;
					}
					tm();
				}
				
			}
		    System.out.println("\n "+name+"のHP："+hp+"\n");
			tm();
		}
		tm();
	    System.out.println("\n決着！\n");
		tm();
		if(t.e_hp==0){
			System.out.println("勝者："+name);
			System.out.println("残りHP："+hp+"\n");
		}else{
			System.out.println("敗北");
	    	tm();
			System.out.println("勝者："+t.e_name);
			System.out.println("残りHP："+t.e_hp);
			System.out.println("残り自分のHP："+hp+"\n");
			if(hp>0){
				System.out.println("敗因：７ターン経過による時間切れ\n");
			}else{
				System.out.println("敗因：HPが０になりました\n");
			}
			hp=0;
    	}
	tm();
	return hp;
	}
	
	public static int atc(int a, int b, int h,String n,String s){
		int d = a-b;
		if(d<0){
			d = 0;
		}
		System.out.println(" "+n+s+d+"ダメージ");
		int hp = h-d;
		if(hp<0){
			hp= 0;
		}
	    return hp;
	}
	public static void tm(){
		try{
			Thread.sleep(1000);
		}catch(Exception e){
			System.out.println("停止に失敗しました。");
		}
	}
}